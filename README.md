# javascript-packages-demo

* [*OpenJS Foundation*
  ](https://en.m.wikipedia.org/wiki/OpenJS_Foundation) (WikipediA)
* https://libraries.io/npm
* https://libraries.io/bower

# Libraries comparisons
## Front-end and interactive web applications
* [*Top 9 Alternatives to ReactJS: Exploring the Best Front-End Frameworks*
  ](https://maybe.works/blogs/alternatives-to-react)
  2024-09 Serhii Holivets (MaybeWorks)

# Popularity
* https://survey.stackoverflow.co/2023/#most-popular-technologies-webframe
## Debian `libjs` libraries
* ![Debian libjs popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=libjs-jquery+libjs-underscore+libjs-mathjax+libjs-jquery-ui&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian libjs popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=libjs-highlight.js+libjs-bootstrap+libjs-inherits+libjs-bootstrap4+libjs-events+libjs-async+libjs-codemirror+libjs-lunr+libjs-bootstrap5&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian libjs popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=libjs-lunr+libjs-requirejs+libjs-backbone+libjs-es6-promise+libjs-d3+libjs-prototype+libjs-leaflet+libjs-scriptaculous+libjs-excanvas+libjs-es5-shim+libjs-cropper&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian libjs popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=libjs-cropper+libjs-katex+libjs-angularjs+libjs-coffeescript+libjs-jquery-jstree+libjs-chart.js+libjs-c3&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* AngularJS: no more supported. See Angular (not yet in Debian 2025).
* ![Debian libjs popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=libjs-c3+libjs-raphael+libjs-dojo-core+libjs-chosen+libjs-elycharts+libjs-filesaver+libjs-impress&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian libjs popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=libjs-impress+libjs-i18next+libjs-vue+libjs-dropzone+libjs-asciimathml+libjs-less+libjs-emojione+libjs-edit-area+libjs-chartkick.js+libjs-debugger&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian libjs popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=libjs-debugger+libjs-favico.js+libjs-handlebars+libjs-headjs+libjs-cryptojs+libjs-dateformat+libjs-flotr&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian libjs popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=libjs-flotr+libjs-ansi-up+libjs-arbiter+libjs-autolink+libjs-autonumeric+libjs-autosize+libjs-awesomplete+libjs-bignumber+libjs-blazy+libjs-browser-request&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian libjs popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=libjs-flotr+libjs-chai+libjs-cssom+libjs-cssrelpreload+libjs-debug+libjs-dygraphs+libjs-emojify+libjs-es6-shim+libjs-eventemitter2+libjs-expect.js+libjs-flatted&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian libjs popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=libjs-flotr+libjs-functional-red-black-tree+libjs-fuzzaldrin-plus+libjs-getobject+libjs-gettext.js+libjs-gordon+libjs-graphael+libjs-hooker+libjs-htmx+libjs-img.srcset+libjs-ipaddr&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

## Debian `node` libraries
* ![Debian node libraries popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=node-prismjs+node-jquery+node-chalk+node-growl+node-highlight.js+node-less&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian node libraries popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=node-less+node-react+node-jquery-ui+node-underscore+node-express&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
* ![Debian node libraries popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=node-react+node-jquery-ui+node-requirejs+node-express+node-vue+node-sqlite3+node-mermaid+node-leaflet+node-lunr+node-ramda&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

# `npm` downloads
## < 1 G
* ![npm-downloads (recent)](https://badgen.net/npm/dm/express) express
  [![NPM](https://nodei.co/npm/express.png)](https://www.npmjs.com/package/express)

## ­< 100 M
* ![npm-downloads (recent)](https://badgen.net/npm/dm/react) react
* ![npm-downloads (recent)](https://badgen.net/npm/dm/jest) jest
* ![npm-downloads (recent)](https://badgen.net/npm/dm/underscore) underscore
  * [*Underscore.js*](https://en.m.wikipedia.org/wiki/Underscore.js)
* ![npm-downloads (recent)](https://badgen.net/npm/dm/jquery) jquery
* ![npm-downloads (recent)](https://badgen.net/npm/dm/ramda) ramda
* ![npm-downloads (recent)](https://badgen.net/npm/dm/highlight.js) highlight.js
* ![npm-downloads (recent)](https://badgen.net/npm/dm/next) next
* ![npm-downloads (recent)](https://badgen.net/npm/dm/@remix-run/router) @remix-run/router
* ![npm-downloads (recent)](https://badgen.net/npm/dm/less) less
* ![npm-downloads (recent)](https://badgen.net/npm/dm/vue) vue
* ![npm-downloads (recent)](https://badgen.net/npm/dm/@nestjs/common) @nestjs/common
  * [*Symfony vs. Nest.js: The Ultimate Showdown for Modern Web Development*
    ](https://vulke.medium.com/symfony-vs-nest-js-the-ultimate-showdown-for-modern-web-development-321d01931387)
    2024-07 Ivan Vulovic (Medium)
* ![npm-downloads (recent)](https://badgen.net/npm/dm/@angular/core) @angular/core
* ![npm-downloads (recent)](https://badgen.net/npm/dm/d3)
  * [*D3.js in Action*
    ](https://manning.com/books/d3js-in-action-third-edition)
    2024-05 (Third Edition) Elijah Meeks, Anne-Marie Dufour (Manning)

## < 10 M
* ![npm-downloads (recent)](https://badgen.net/npm/dm/lunr) lunr
* ![npm-downloads (recent)](https://badgen.net/npm/dm/lit) lit
* ![npm-downloads (recent)](https://badgen.net/npm/dm/fastify) fastify
* ![npm-downloads (recent)](https://badgen.net/npm/dm/svelte) svelte
  * *Hands-on JavaScript high performance : build faster web apps using Node. js, Svelte. js, and webAssembly*
    **2020** Justin Scherer (Packt)
* ![npm-downloads (recent)](https://badgen.net/npm/dm/leaflet) leaflet
* ![npm-downloads (recent)](https://badgen.net/npm/dm/katex) katex
* ![npm-downloads (recent)](https://badgen.net/npm/dm/jquery-ui) jquery-ui
* ![npm-downloads (recent)](https://badgen.net/npm/dm/mermaid) mermaid
* ![npm-downloads (recent)](https://badgen.net/npm/dm/nuxt) nuxt
* ![npm-downloads (recent)](https://badgen.net/npm/dm/@remix-run/node) @remix-run/node
* ![npm-downloads (recent)](https://badgen.net/npm/dm/backbone) backbone
  * [*Backbone.js*](https://en.m.wikipedia.org/wiki/Backbone.js)
* ![npm-downloads (recent)](https://badgen.net/npm/dm/solid-js) solid-js
* ![npm-downloads (recent)](https://badgen.net/npm/dm/gatsby) gatsby

## < 1 M
* ![npm-downloads (recent)](https://badgen.net/npm/dm/@hotwired/turbo) @hotwired/turbo
* ![npm-downloads (recent)](https://badgen.net/npm/dm/@remix-run/serve) @remix-run/serve
* ![npm-downloads (recent)](https://badgen.net/npm/dm/mathjax) mathjax
* ![npm-downloads (recent)](https://badgen.net/npm/dm/raphael) raphael
  * *RaphaelJS : graphics and visualization on the web*
    2013 Chris Wilson (O'Reilly)
* ![npm-downloads (recent)](https://badgen.net/npm/dm/typed.js) typed.js
* ![npm-downloads (recent)](https://badgen.net/npm/dm/jstree) (jquery-)jstree
* ![npm-downloads (recent)](https://badgen.net/npm/dm/assemblyscript) assemblyscript

## < 100 K
* ![npm-downloads (recent)](https://badgen.net/npm/dm/swup) swup
* ![npm-downloads (recent)](https://badgen.net/npm/dm/purescript) purescript
* ![npm-downloads (recent)](https://badgen.net/npm/dm/@plesk/prototype.js) @plesk/prototype.js
  * [*Prototype_JavaScript_Framework*
    ](https://en.m.wikipedia.org/wiki/Prototype_JavaScript_Framework)
* ![npm-downloads (recent)](https://badgen.net/npm/dm/)

## Not in npm
* [alpinejs/alpine](https://github.com/alpinejs/alpine)
  * [*Htmx and alpine.js a good fit?*
    ](https://www.reddit.com/r/htmx/comments/1ajut2u/htmx_and_alpinejs_a_good_fit/)
    (**2024**) (Reddit)
* [bigskysoftware/htmx](https://github.com/bigskysoftware/htmx)
  * [Htmx](https://en.wikipedia.org/wiki/Htmx) (WikipediA)
  * [Repology](https://repology.org/project/htmx/versions)
  * [Haskell lucid](https://hackage.haskell.org/package/lucid-htmx) and also other htmx Haskell packages.
  * [*A modest critique of Htmx*
    ](https://chrisdone.com/posts/htmx-critique)
    **2024**-08 Chris Done
* [bigskysoftware/_hyperscript](https://github.com/bigskysoftware/_hyperscript)

# Libraries by topic
## Simulate a pseudo shell in browser
* ![npm-downloads (recent)](https://badgen.net/npm/dm/terminal.js)[terminal.js](https://www.npmjs.com/package/terminal.js)
* ![npm-downloads (recent)](https://badgen.net/npm/dm/terminal-js-emulator)[terminal-js-emulator](https://www.npmjs.com/package/terminal-js-emulator)
* https://www.masswerk.at/termlib
  * [*How to make a full screen terminal with termlib.js?*
    ](https://stackoverflow.com/questions/6099777/how-to-make-a-full-screen-terminal-with-termlib-js)
